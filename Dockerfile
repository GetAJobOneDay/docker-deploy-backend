from openjdk:8-jdk-alpine
arg JAVA_FILE
copy ${JAVA_FILE} app.jar
expose 8080

entrypoint ["java","-Djava.security.egd=file:/dev/./urandom","-Dspring.profiles.active=dev-server","-jar","/app.jar"]
